# Super User

A webapp to test word pairs with the design of the Prototype Fund.

[Try it out!](https://jaller94.gitlab.io/super-user)

## How to use

Open `public/index.html` in you browser. You may even save this to your desktop and open it from there.

Use the Tab button to focus the overlapping text fields. You may also use the buttons below the text to focus the text fields.

Change the words in the pink and blue text fields until you found a pair that looks good.

## Features that could be cool
* [ ] SVG export

## License
[MIT](./LICENSE)
